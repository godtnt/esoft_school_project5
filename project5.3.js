const gameFeld = [
    ['x', 'o', 'o'],
    ['x', 'o', 'x'],
    ['o', 'x', 'x']
]

let winX = null
let winO = null

function winXCondition (arrey) {
    const firstCell = arrey[0]; const secondCell = arrey[1]; const thirdCell = arrey[2]; const fourthCell = arrey[3]; const fifthCell = arrey[4]; const sixthCell = arrey[5]; const seventhCell = arrey[6]; const eighthCell = arrey[7]; const ninthCell = arrey[8];
    if ((firstCell == 'x' && secondCell == 'x' && thirdCell == 'x') || (fourthCell == 'x' && fifthCell == 'x' && sixthCell == 'x') || (seventhCell == 'x' && eighthCell == 'x' && ninthCell == 'x') || (firstCell == 'x' && fourthCell == 'x' && seventhCell == 'x') || (secondCell == 'x' && fifthCell == 'x' && eighthCell == 'x') || (thirdCell == 'x' && sixthCell == 'x' && ninthCell == 'x') || (firstCell == 'x' && fifthCell == 'x' && ninthCell == 'x') || (thirdCell == 'x' && fifthCell == 'x' && seventhCell == 'x')) {
        winX = true
    } else {
        winX = false
    }
    return winX
}
function winOCondition (arrey) {
    const firstCell = arrey[0]; const secondCell = arrey[1]; const thirdCell = arrey[2]; const fourthCell = arrey[3]; const fifthCell = arrey[4]; const sixthCell = arrey[5]; const seventhCell = arrey[6]; const eighthCell = arrey[7]; const ninthCell = arrey[8];
    if ((firstCell == 'o' && secondCell == 'o' && thirdCell == 'o') || (fourthCell == 'o' && fifthCell == 'o' && sixthCell == 'o') || (seventhCell == 'o' && eighthCell == 'o' && ninthCell == 'o') || (firstCell == 'o' && fourthCell == 'o' && seventhCell == 'o') || (secondCell == 'o' && fifthCell == 'o' && eighthCell == 'o') || (thirdCell == 'o' && sixthCell == 'o' && ninthCell == 'o') || (firstCell == 'o' && fifthCell == 'o' && ninthCell == 'o') || (thirdCell == 'o' && fifthCell == 'o' && seventhCell == 'o')) {
        winO = true
    } else {
        winO = false
    }
    return winO
}

const winOrLossX = (winXCondition(gameFeld.flat()))
const winOrLossO = (winOCondition(gameFeld.flat()))

if (gameFeld.flat().filter(val => val != null).length !== 9) {
    console.log('Игра не окончена')
} else if (winOrLossX == true && winOrLossO == true) {
    console.log('Ничья')
} else if (winOrLossX == true) {
    console.log('Крестики победили')
} else if (winOrLossO == true) {
    console.log('Нолики победили')
} else {
    console.log('Ничья')
}